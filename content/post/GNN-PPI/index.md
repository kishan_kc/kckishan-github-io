+++
title = "Graph Neural Network for PPI prediction"
subtitle = ""

# Add a summary to display on homepage (optional).
summary = ""

date = 2019-02-22T19:40:14-05:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Kishan KC"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["GNN"]
categories = ["deep-learning", "cb"]

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
# projects = ["internal-project"]

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

# Protein-Protein Interaction
Proteins are the functional unit within an organism that form molecular machines organized by their protein-protein interactions (PPIs) to carry out many biological and molecular processes. The study of PPIs not only plays a key role in understanding biological phenomenon but also provides insights about the molecular etiology of diseases as well as the discovery of putative drug targets. 


```python

```


```python

```
